# My Potatogotchi

## What is it

This project is a "Tamagotchi" in interactive mode or console mode.
It is made with SCSS/HTML5/JS and can be seen live [here](https://www.potatogotchi.vivianetixier.com).

The goal is to make his little potato survive, take care of its needs (sleep, feed, wash).
If one of those needs are at zero, then your potatogotchi will die (game over).

Interactive mode           |  Console mode
:-------------------------:|:-------------------------:
[![In interactive mode](example_interactive.png)](https://www.potatogotchi.vivianetixier.com)  |  [![In console mode](example_console.png)](https://www.potatogotchi.vivianetixier.com/potatogotchiV2.html)


## How to build

 - Install NPM if you don't have it

```
curl -sL https://deb.nodesource.com/setup_10.x | bash -
apt-get -y -qq install nodejs > /dev/null
```

 - Get SASS from NPM

`npm install -g sass`

 - Build the project (from root directory)

`sass assets/css/style.scss:assets/css/style.css`

 - Launch the `index.html` file to see the project running


## Potential improvements

 - [ ] Allow level-up
 - [ ] Evolve the potato (with level-up)
 - [x] Show score
 - [ ] Add ranking with username


## License

This project is under MIT license. This means you can use it as you want (credit me please).
