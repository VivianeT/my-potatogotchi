// Variables used in all the application
let gSleep = 100;
let gHunger = 100;
let gHygiene = 100;
let gIsAlive = true;//Patate est vivante
const dodo = document.querySelector('#dodo');
const nourrir = document.querySelector('#nourrir');
const laver = document.querySelector('#laver');
const reset = document.querySelector('#reset');
let speed = 1001;//Vitesse du timer
const accel = 10;//Vitesse accélération
let score = 0;
const start = document.getElementById('start');

//Score
const majScore = document.getElementById('score');

let timerSleep;
let timerHunger;
let timerHygiene;

//Fonction Sleep

function myTimerSleep() {
    //gérer de 100 à 0
    if (gSleep > 0) {
        gSleep--;
    }
    document.getElementById('sleepNumber').innerText = 'Sommeil : ' + gSleep;

    let sleepText = '';

    if (gSleep <= 30 && gSleep > 10) {
        sleepText = 'Potato pleure, elle a sommeil';
    }
    else if (gSleep <= 10 && gSleep >= 1) {
        sleepText = 'Potato déprime, elle va tomber de sommeil';
    }
    else if (gSleep == 0) {
        gIsAlive = false;
        document.getElementById('potatoDeath').innerText = 'Ta patate est morte. Ton Score est de ' + score + ' !';
        reset.style.visibility = 'visible';
        sleepText = '';
        hygieneText = '';
        hungerText = '';
    }
    document.getElementById('sleepStatus').innerText = sleepText;

    if (gIsAlive == true) {
        timerSleep = setTimeout(myTimerSleep, speed);
    }
}

//Fonction Hunger

function myTimerHunger() {
    //gérer de 100 à 0
    if (gHunger > 0) {
        gHunger--;
    }
    document.getElementById('hungerNumber').innerText = 'Faim : ' + gHunger;

    let hungerText = '';

    if (gHunger <= 30 && gHunger > 10) {
        hungerText = 'Potato pleure, elle a faim';
    }
    else if (gHunger <= 10 && gHunger >= 1) {
        hungerText = 'Potato déprime, elle crève la dalle';
    }
    else if (gHunger == 0) {
        gIsAlive = false;
        document.getElementById('potatoDeath').innerText = 'Ta patate est morte. Ton Score est de ' + score + ' !';
        reset.style.visibility = 'visible';
        hungerText = '';
        hygieneText = '';
        sleepText = '';
    }
    document.getElementById('hungerStatus').innerText = hungerText;

    if (gIsAlive == true) {
        timerHunger = setTimeout(myTimerHunger, speed - 100);
    }
}

//Fonction Hygiene

function myTimerHygiene() {
    //gérer de 100 à 0
    if (gHygiene > 0) {
        gHygiene--;
    }
    document.getElementById('hygieneNumber').innerText = 'Hygiène : ' + gHygiene;

    let hygieneText = '';

    if (gHygiene <= 30 && gHygiene > 10) {
        hygieneText = 'Potato pleure, elle pue';
    }
    else if (gHygiene <= 10 && gHygiene >= 1) {
        hygieneText = 'Potato déprime, elle s\'étouffe dans son caca';
    }
    else if (gHygiene == 0) {
        gIsAlive = false;
        document.getElementById('potatoDeath').innerText = 'Ta patate est morte. Ton Score est de ' + score + ' !';
        reset.style.visibility = 'visible';
        hygieneText = '';
        hungerText = '';
        sleepText = '';
    }
    document.getElementById('hygieneStatus').innerText = hygieneText;

    if (gIsAlive == true) {
        timerHygiene = setTimeout(myTimerHygiene, speed - 200);
    }
}

//Initialisation des timer

function play() {
    clearTimeout(timerSleep);
    clearTimeout(timerHunger);
    clearTimeout(timerHygiene);
    timerSleep = setTimeout(myTimerSleep, 1001);
    timerHunger = setTimeout(myTimerHunger, 1001);
    timerHygiene = setTimeout(myTimerHygiene, 1001);
}

//Start Button
start.addEventListener('click', play);

//Reset Button 
reset.style.visibility = 'hidden';
reset.addEventListener('click', function () {
    reset.style.visibility = 'hidden';
    timerSleep = setTimeout(myTimerSleep, 1001);
    timerHunger = setTimeout(myTimerHunger, 1001);
    timerHygiene = setTimeout(myTimerHygiene, 1001);
    gSleep = 100;
    gHunger = 100;
    gHygiene = 100;
    gIsAlive = true;
    majScore.innerText = 'Score : ' + 0 + '  pt';
    score = 0;
    speed = 1001;
    document.getElementById('potatoDeath').innerText = 'Encore une fois !';
});

//Bouton dormir V1

dodo.addEventListener('click', function () {
    if ((gIsAlive = true) && (gSleep <= 50)) {
        gSleep = 100;
        score++;
        majScore.innerText = 'Score : ' + score + ' pts';
        if (speed > 280) {
            speed = speed - accel;
        }
    }
});

//Bouton nourrir V1

nourrir.addEventListener('click', function () {
    if ((gIsAlive = true) && (gHunger <= 50)) {
        gHunger = 100;
        score++;
        majScore.innerText = 'Score : ' + score + ' pts';
        if (speed > 280) {
            speed = speed - accel;
        }
    }
});

//Bouton laver V1

laver.addEventListener('click', function () {
    if ((gIsAlive = true) && (gHygiene <= 50)) {
        gHygiene = 100;
        score++;
        majScore.innerText = 'Score : ' + score + ' pts';
        if (speed > 280) {
            speed = speed - accel;
        }
    }
});

