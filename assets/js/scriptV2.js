// Variables used in all the application
let sleep = document.querySelector('#sommeilProgress span.sommeilBarre');
let wSleep = sleep.style.width;
let gSleep = 100;
let hunger = document.querySelector('#faimProgress span.faimBarre');
let wHunger = hunger.style.width;
let gHunger = 100;
let hygiene = document.querySelector('#hygieneProgress span.hygieneBarre');
let wHygiene = hygiene.style.width;
let gHygiene = 100;
let gIsAlive = true;//Patate est vivante
const reset = document.querySelector('#reset');
let speed = 1001;//Vitesse du timer
const accel = 50;//Vitesse accélération
let score = 0;
const start = document.getElementById('start');

//Score
const majScore = document.getElementById('score');

//Initialiser les timer
let timerSleep;
let timerHunger;
let timerHygiene;

//Fonction Sleep

function myTimerSleep() {
    //gérer de 100 à 0
    let alertSleep = document.getElementById('sSleep');

    if (gSleep > 0) {
        gSleep--;
        sleep.style.width = gSleep + '%';
        alertSleep.removeAttribute('class', 'active-sleep');
        alertSleep.innerText = '';
    }
    if (gSleep <= 50 && gSleep >= 1) {
        alertSleep.setAttribute('class', 'active-sleep');
        alertSleep.innerText = 'J\'ai sommeil';
    }
    else if (gSleep == 0) {
        gIsAlive = false;
        document.getElementById('potatoDeath').innerText = 'Ta patate est morte. Ton Score est de ' + score + ' !';
        reset.style.visibility = 'visible';
        let soundDeath = document.getElementById('dcd');
        soundDeath.currentTime = 0;
        soundDeath.play();
    }
    if (gIsAlive == true) {
        timerSleep = setTimeout(myTimerSleep, speed);
    }
    if (gIsAlive == false) {
        alertSleep.innerText = '';
        alertSleep.removeAttribute('class', 'active-sleep');
    }
}

//Fonction Hunger

function myTimerHunger() {
    //gérer de 100 à 0
    let alertFaim = document.getElementById('sFaim');

    if (gHunger > 0) {
        gHunger--;
        hunger.style.width = gHunger + '%';
        alertFaim.removeAttribute('class', 'active-faim');
        alertFaim.innerText = '';
    }
    if (gHunger <= 50 && gHunger >= 1) {
        alertFaim.setAttribute('class', 'active-faim');
        alertFaim.innerText = 'J\'ai faim';
    }
    else if (gHunger == 0) {
        gIsAlive = false;
        document.getElementById('potatoDeath').innerText = 'Ta patate est morte. Ton Score est de ' + score + ' !';
        reset.style.visibility = 'visible';
        let soundDeath = document.getElementById('dcd');
        soundDeath.currentTime = 0;
        soundDeath.play();
    }
    if (gIsAlive == true) {
        timerHunger = setTimeout(myTimerHunger, speed - 100);
    }
    if (gIsAlive == false) {
        alertFaim.innerText = '';
        alertFaim.removeAttribute('class', 'active-faim');
    }
}

//Fonction Hygiene

function myTimerHygiene() {
    //gérer de 100 à 0
    let alertHyg = document.getElementById('sHyg');

    if (gHygiene > 0) {
        gHygiene--;
        hygiene.style.width = gHygiene + '%';
        alertHyg.removeAttribute('class', 'active-hyg');
        alertHyg.innerText = '';
    }
    if (gHygiene <= 50 && gHygiene >= 1) {
        alertHyg.setAttribute('class', 'active-hyg');
        alertHyg.innerText = 'Je sens mauvais';
    }
    else if (gHygiene == 0) {
        gIsAlive = false;
        document.getElementById('potatoDeath').innerText = 'Ta patate est morte. Ton Score est de ' + score + ' !';
        reset.style.visibility = 'visible';
        let soundDeath = document.getElementById('dcd');
        soundDeath.currentTime = 0;
        soundDeath.play();
    }
    if (gIsAlive == true) {
        timerHygiene = setTimeout(myTimerHygiene, speed - 200);
    }
    if (gIsAlive == false) {
        alertHyg.innerText = '';
        alertHyg.removeAttribute('class', 'active-hyg');
    }
}

//Lancer les timer
function play() {
    clearTimeout(timerSleep);
    clearTimeout(timerHunger);
    clearTimeout(timerHygiene);
    timerSleep = setTimeout(myTimerSleep, 1001);
    timerHunger = setTimeout(myTimerHunger, 1001);
    timerHygiene = setTimeout(myTimerHygiene, 1001);
}
//Start Button
start.addEventListener('click', play);

//Reset Button 
reset.style.visibility = 'hidden';
reset.addEventListener('click', function () {
    reset.style.visibility = 'hidden';
    timerSleep = setTimeout(myTimerSleep, 1001);
    timerHunger = setTimeout(myTimerHunger, 1001);
    timerHygiene = setTimeout(myTimerHygiene, 1001);
    gSleep = 100;
    gHunger = 100;
    gHygiene = 100;
    gIsAlive = true;
    majScore.innerText = 'Score : ' + 0 + '  pt';
    score = 0;
    speed = 1001;
    document.getElementById('potatoDeath').innerText = '';
});

//Gérer la console intégrée 

let consoleText = document.getElementById('consoleText');
const submitText = document.getElementById('consoleValid');
let consoleForm = document.getElementById('console');

//Click sur le bouton d'envoi
consoleForm.addEventListener('submit', function (event) {
    event.preventDefault();

    if (gIsAlive == true) {
        //Pour le sommeil
        if (consoleText.value == 'make sleep;') {
            if (gSleep <= 50) {
                gSleep = 100;
                score++;
                consoleText.value = '';
                majScore.innerText = 'Score : ' + score + ' pts';
                let soundSleep = document.getElementById('ronfle');
                soundSleep.currentTime = 0;
                soundSleep.play();
                if (speed > 350) {
                    speed = speed - accel;
                }
            }
        }
        //Pour la faim
        if (consoleText.value == 'feed;') {
            if (gHunger <= 50) {
                gHunger = 100;
                score++;
                consoleText.value = '';
                majScore.innerText = 'Score : ' + score + ' pts';
                let soundHunger = document.getElementById('miam');
                soundHunger.currentTime = 0;
                soundHunger.play();
                if (speed > 350) {
                    speed = speed - accel;
                }
            }
        }
        //Pour l'hygiène
        if (consoleText.value == 'wash;') {
            if (gHygiene <= 50) {
                gHygiene = 100;
                score++;
                consoleText.value = '';
                majScore.innerText = 'Score : ' + score + ' pts';
                let soundHygiene = document.getElementById('popo');
                soundHygiene.currentTime = 0;
                soundHygiene.play();
                if (speed > 350) {
                    speed = speed - accel;
                }
            }
        }
    }

});

